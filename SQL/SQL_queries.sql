#2.1
SELECT CUSTOMERS.NAME, MANAGERS.NAME
FROM CUSTOMERS
JOIN MANAGERS ON MANAGERS.ID = CUSTOMERS.MANAGER_ID
JOIN ORDERS ON ORDERS.CUSTOMER_ID = CUSTOMERS.ID
WHERE ORDERS.AMOUNT > 10000
AND ORDERS.DATE >= '2013-01-01';

#2.2
SELECT e.name AS 'emp_name'
FROM employees AS e
WHERE e.name LIKE '%m%' OR e.name LIKE '%M%';
 
SELECT d.id AS 'dept_id', MAX(e.salary) AS 'salary'
FROM   departments AS d
JOIN employees AS e ON e.department_id = d.id
GROUP BY d.name;

SELECT e.name AS 'emp_name', e.salary AS 'salary'
FROM employees AS e
WHERE e.salary IN 
		(SELECT e.salary
		FROM employees AS e
		GROUP BY e.salary
		HAVING COUNT(*)>1);