﻿using System;
using System.Collections.Generic;
using System.Linq;
using TechnicalTask.Models;

namespace TechnicalTask
{
    public class CollectionHandler
    {
        public List<Individual> Individuals { get; set; }
        public List<JuridicalPerson> JuridicalPeople { get; set; }
        
        public List<Individual> GenerateIndividuals()
        {
            return new List<Individual>()
            {
                new Individual()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "123456789000",
                    CreationDate = DateTime.Now.AddDays(-100),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Alex",
                    Surname = "Berger",
                    MiddleName = "Winston",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-16)
                },
                new Individual()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "123456789001",
                    CreationDate = DateTime.Now.AddDays(-95),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "William",
                    Surname = "Clark",
                    MiddleName = "James",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-25)
                },
                new Individual()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "123456789002",
                    CreationDate = DateTime.Now.AddDays(-88),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Derry",
                    Surname = "Brown",
                    MiddleName = "Joe",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-4)
                },
                new Individual()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "123456789003",
                    CreationDate = DateTime.Now.AddDays(-74),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Alice",
                    Surname = "Watson",
                    MiddleName = "Thomas",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-43)
                },
                new Individual()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "223456789003",
                    CreationDate = DateTime.Now.AddDays(-72),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Ann",
                    Surname = "Stewart",
                    MiddleName = "Reynold",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-40)
                },
                new Individual()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "323456789003",
                    CreationDate = DateTime.Now.AddDays(-67),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Emily",
                    Surname = "Stevenson",
                    MiddleName = "Ford",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-33)
                },
                new Individual()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "423456789003",
                    CreationDate = DateTime.Now.AddDays(-52),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Vanessa",
                    Surname = "Thomas",
                    MiddleName = "Clark",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-23)
                },
            };
        }

        public List<JuridicalPerson> GenerateJuridicalPeople()
        {
            return new List<JuridicalPerson>()
            {
                new JuridicalPerson()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "123456789004",
                    CreationDate = DateTime.Now.AddDays(-106),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Fish Corp.",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-12),
                    Contacts = GenerateIndividuals().GetRange(0, 2)
                },
                new JuridicalPerson()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "123456789005",
                    CreationDate = DateTime.Now.AddDays(-85),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Pizza House",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-18),
                    Contacts = GenerateIndividuals().GetRange(0, 3)
                },
                new JuridicalPerson()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "123456789006",
                    CreationDate = DateTime.Now.AddDays(-74),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Fashion City",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-11),
                    Contacts = GenerateIndividuals().GetRange(2, 1)
                },
                new JuridicalPerson()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "123456789007",
                    CreationDate = DateTime.Now.AddDays(-63),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Fast Taxi",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-9),
                    Contacts = GenerateIndividuals().GetRange(0, 4)
                },
                new JuridicalPerson()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "123456789008",
                    CreationDate = DateTime.Now.AddDays(-52),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Healthy City",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-10),
                    Contacts = GenerateIndividuals().GetRange(1, 5)
                },
                new JuridicalPerson()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "123456789009",
                    CreationDate = DateTime.Now.AddDays(-41),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Global Tour Agency",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-7),
                    Contacts = GenerateIndividuals().GetRange(0, 7)
                },
                new JuridicalPerson()
                {
                    Id = Guid.NewGuid().ToString(),
                    Bin = "123456789010",
                    CreationDate = DateTime.Now.AddDays(-38),
                    CreatorId = Guid.NewGuid().ToString(),
                    Name = "Sushi bar",
                    EditorId = Guid.NewGuid().ToString(),
                    EditingDate = DateTime.Now.AddDays(-5),
                    Contacts = GenerateIndividuals().GetRange(0, 6)
                }
            };
        }

        public List<Individual> OrderIndividualsBySurnameNameAndMiddleName(List<Individual> collection)
        {
            return collection.OrderBy(i => i.Surname).ThenBy(i => i.Name).ThenBy(i => i.MiddleName).ToList();
        }

        public List<JuridicalPerson> OrderJuridicalByContactsQtyDesc(List<JuridicalPerson> collection)
        {
            return collection.OrderByDescending(jp => jp.Contacts.Count).ToList();
        }
    }
}