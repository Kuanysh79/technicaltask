﻿using System;
using System.Collections.Generic;
using TechnicalTask.Models;

namespace TechnicalTask
{
    public class Configurator
    {
        public void Configure()
        {
            CollectionHandler collectionHandler = new CollectionHandler();
            FileHandler fileHandler = new FileHandler(collectionHandler);
            fileHandler.FileWriter();
            fileHandler.FileReader();
            var individualsOrdered =
                collectionHandler.OrderIndividualsBySurnameNameAndMiddleName(collectionHandler.Individuals);
            var juridicOrdered = collectionHandler.OrderJuridicalByContactsQtyDesc(collectionHandler.JuridicalPeople).GetRange(0, 5);
            WriteCollection<Individual>(individualsOrdered);
            WriteCollection<JuridicalPerson>(juridicOrdered);
        }

        private void WriteCollection<T>(List<T> collection)
        {
            if (collection.GetType() == typeof(List<Individual>))
            {
                Console.WriteLine($"{Environment.NewLine}" +
                                  "Ordered by surname, name and middle name list of individuals:" +
                                  $"{Environment.NewLine}");
            }
            else
            {
                Console.WriteLine($"{Environment.NewLine}" +
                                  "Ordered by descending contacts quantity list of juridic people:" +
                                  $"{Environment.NewLine}");
            }
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
        }
    }
}