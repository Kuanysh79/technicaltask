﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using TechnicalTask.Models;

namespace TechnicalTask
{
    public class FileHandler
    {
        private const string PathForIndividuals = "../../../Files/individuals.json";
        private const string PathForJuridicalPeople = "../../../Files/juridical.json";
        
        private readonly CollectionHandler _collectionHandler;

        public FileHandler(CollectionHandler collectionHandler)
        {
            _collectionHandler = collectionHandler;
        }


        public void FileWriter()
        {
            try
            {
                string individuals = Serialize(_collectionHandler.GenerateIndividuals());
                File.WriteAllText(PathForIndividuals, individuals);
                string juridicalPeople = Serialize(_collectionHandler.GenerateJuridicalPeople());
                File.WriteAllText(PathForJuridicalPeople, juridicalPeople);
            }
            catch (IOException)
            {
                Console.WriteLine("Не удалось открыть файл(-ы), проверьте расширения файл(-ов)");
                throw;
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Путь до файла содержит пробелы или не поддерживающиеся символы");
                throw;
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("Проверьте правильность указания путей, возможно, " +
                                  "запрашиваемый файл является исполняемым и используется на данный момент, " +
                                  "либо файл доступен только для чтения, " +
                                  "либо путь указан к каталогу и не включает название файла с указанием расширения");
                throw;
            }
            catch (NotSupportedException)
            {
                Console.WriteLine("Путь до файла указан в неверном формате");
                throw;
            }
        }

        public void FileReader()
        {
            try
            {
                string contentForIndividuals = File.ReadAllText(PathForIndividuals);
                string contentForJuridical = File.ReadAllText(PathForJuridicalPeople);
                _collectionHandler.Individuals = Deserialize<Individual>(contentForIndividuals);
                _collectionHandler.JuridicalPeople = Deserialize<JuridicalPerson>(contentForJuridical);
            }
            catch (IOException)
            {
                Console.WriteLine("Не удалось открыть файл(-ы), проверьте расширения файл(-ов)");
                throw;
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Путь до файла содержит пробелы или не поддерживающиеся символы");
                throw;
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("Проверьте правильность указания путей, возможно, " +
                                  "запрашиваемый файл является исполняемым и используется на данный момент, " +
                                  "либо файл доступен только для чтения, " +
                                  "либо путь указан к каталогу и не включает название файла с указанием расширения");
                throw;
            }
            catch (NotSupportedException)
            {
                Console.WriteLine("Путь до файла указан в неверном формате");
                throw;
            }
        }

        private static string Serialize<T>(List<T> collection)
        {
            try
            {
                var options = new JsonSerializerOptions
                {
                    Encoder = JavaScriptEncoder.Create(UnicodeRanges.All),
                    WriteIndented = true
                };
                return JsonSerializer.Serialize(collection, options);
            }
            catch (NotSupportedException)
            {
                Console.WriteLine("Переданные данные не совместимы с сериализатором");
                throw;
            }
        }

        private static List<T> Deserialize<T>(string fileContent)
        {
            try
            {
                var options = new JsonSerializerOptions
                {
                    Encoder = JavaScriptEncoder.Create(UnicodeRanges.All),
                    WriteIndented = true
                };
                return JsonSerializer.Deserialize<List<T>>(fileContent, options);
            }
            catch (NotSupportedException)
            {
                Console.WriteLine("Переданные данные не совместимы с десериализатором");
                throw;
            }
            catch (ArgumentNullException)
            {
                Console.WriteLine("JSON файл ничего не содержит");
                throw;
            }
        }
    }
}