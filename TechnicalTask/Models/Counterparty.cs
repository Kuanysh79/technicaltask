﻿using System;

namespace TechnicalTask.Models
{
    public class Counterparty
    {
        public string Id { get; set; }
        public string Bin { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreatorId { get; set; }
        public DateTime EditingDate { get; set; }
        public string EditorId { get; set; }
    }
}