﻿using System;

namespace TechnicalTask.Models
{
    public class Individual : Counterparty
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string MiddleName { get; set; }
        
        public override string ToString()
        {
            return $"Id - {Id}{Environment.NewLine}" +
                   $"BIN - {Bin}{Environment.NewLine}" +
                   $"Name - {Name}{Environment.NewLine}" +
                   $"Surname - {Surname}{Environment.NewLine}" +
                   $"Middle name - {MiddleName}{Environment.NewLine}" +
                   $"Creation date - {CreationDate.ToShortDateString()}{Environment.NewLine}" +
                   $"Editing date - {EditingDate.ToShortDateString()}{Environment.NewLine}" +
                   $"Creator Id - {CreatorId}{Environment.NewLine}" +
                   $"Editor Id - {EditorId}{Environment.NewLine}";
        }
    }
}