﻿using System;
using System.Collections.Generic;

namespace TechnicalTask.Models
{
    public class JuridicalPerson : Counterparty
    {
        public string Name { get; set; }
        public List<Individual> Contacts { get; set; }

        public override string ToString()
        {
            return $"Id - {Id}{Environment.NewLine}" +
                   $"BIN - {Bin}{Environment.NewLine}" +
                   $"Name - {Name}{Environment.NewLine}" +
                   $"Creation date - {CreationDate.ToShortDateString()}{Environment.NewLine}" +
                   $"Editing date - {EditingDate.ToShortDateString()}{Environment.NewLine}" +
                   $"Creator Id - {CreatorId}{Environment.NewLine}" +
                   $"Editor Id - {EditorId}{Environment.NewLine}" +
                   $"Contacts quantity - {Contacts.Count}{Environment.NewLine}";

        }
    }
}