﻿using System;

namespace TechnicalTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Configurator configurator = new Configurator();
            configurator.Configure();
        }
    }
}